import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:riverpod_playground/00_state_provider.dart';

void main() {
  testWidgets('Tapping the button once increments by 1', (tester) async {
    // Wrap widget under test in ProviderScope
    await tester.pumpWidget(ProviderScope(child: MyApp()));

    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    expect(find.text('1'), findsOneWidget);
    expect(find.text('0'), findsNothing);
  });

  testWidgets('Tapping the button twice increments by 2', (tester) async {
    // State is not shared between tests. Providers are global, but state is
    // not. It's stored in a ProviderContainer internally.
    await tester.pumpWidget(ProviderScope(child: MyApp()));

    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    await tester.tap(find.byIcon(Icons.add));
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    expect(find.text('2'), findsOneWidget);
    expect(find.text('1'), findsNothing);
    expect(find.text('0'), findsNothing);
  });

  test('Test provider outside of widget', () {
    // Use ProviderContainer to access state.
    final container = ProviderContainer();
    addTearDown(container.dispose);

    expect(container.read(counterProvider).state, equals(0));
    container.read(counterProvider).state++;
    expect(container.read(counterProvider).state, equals(1));
  });

  testWidgets('Override provider for testing', (tester) async {
    await tester.pumpWidget(
      ProviderScope(
        // Override provider
        overrides: [
          counterProvider.overrideWithValue(StateController(42)),
        ],
        child: MyApp(),
      ),
    );

    expect(find.text('42'), findsOneWidget);
  });
}
