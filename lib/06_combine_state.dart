import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(
    const ProviderScope(child: MyApp()),
  );
}

final counterProvider = StateProvider((ref) => 0);

final randomNumberProvider = StreamProvider<int>((ref) async* {
  final rng = Random();
  while (true) {
    await Future.delayed(Duration(seconds: 1));
    yield rng.nextInt(10) + 1;
  }
});

final resultProvider = Provider<int>((ref) {
  // You can depend on other providers
  final count = ref.watch(counterProvider).state;
  final randomNumber = ref.watch(randomNumberProvider).data;
  return randomNumber?.when(
        loading: () => 0,
        data: (randomNumber) => count % randomNumber,
        error: (err, stack) => 0,
      ) ??
      0;
});

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home());
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('Combine State')),
        body: Center(
          child: Consumer(
            builder: (context, watch, _) {
              final count = watch(counterProvider).state;
              final randomNumber = watch(randomNumberProvider).data;
              final result = watch(resultProvider);
              return randomNumber?.when(
                    loading: () => const CircularProgressIndicator(),
                    data: (randomNumber) => Text(
                      '$count % $randomNumber = $result',
                      style: TextStyle(fontSize: 24),
                    ),
                    error: (err, stack) => Text('Error $err'),
                  ) ??
                  const CircularProgressIndicator();
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => context.read(counterProvider).state++,
          child: const Icon(Icons.add),
        ),
      );
}
