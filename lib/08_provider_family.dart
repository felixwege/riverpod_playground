import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(
    const ProviderScope(child: MyApp()),
  );
}

final scopedProvider = ScopedProvider<int>((_) => throw UnimplementedError());
final providerFamily = Provider.family<int, int>((ref, x) {
  return x * x;
});

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home());
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('Provider Family Example')),
        body: Container(
          padding: EdgeInsets.all(32),
          child: ListView.builder(
            itemCount: 10,
            itemBuilder: (context, index) => ProviderScope(
              overrides: [
                scopedProvider.overrideWithValue(index),
              ],
              child: ListItem(),
            ),
          ),
        ),
      );
}

class ListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Consumer(
        builder: (context, watch, _) {
          final index = watch(scopedProvider);
          final squared = watch(providerFamily(index));
          return Text(
            '$squared',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24),
          );
        },
      );
}
