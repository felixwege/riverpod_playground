import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

void main() {
  runApp(
    const ProviderScope(child: MyApp()),
  );
}

// Model state and mutations with StateNotifier
class Counter extends StateNotifier<int> {
  Counter() : super(0);

  void increment() => state++;
}

// Use StateNotifierProvider
final counterProvider = StateNotifierProvider<Counter>((_) => Counter());

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Home(),
    );
  }
}

class Home extends HookWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final count = useProvider(counterProvider.state);
    return Scaffold(
      appBar: AppBar(title: const Text('StateNotifier Example')),
      body: Center(
        child: Text(
          '$count',
          style: TextStyle(fontSize: 24),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        // Call methods to modify state instead of setting it directly
        onPressed: () => context.read(counterProvider).increment(),
        child: const Icon(Icons.add),
      ),
    );
  }
}
