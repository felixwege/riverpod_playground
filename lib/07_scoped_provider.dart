import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(
    const ProviderScope(child: MyApp()),
  );
}

final scopedProvider = ScopedProvider<int>((_) => throw UnimplementedError());

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home());
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('ScopedProvider Example')),
        body: Container(
          padding: EdgeInsets.all(32),
          child: ListView.builder(
            itemCount: 10,
            itemBuilder: (context, index) => ProviderScope(
              overrides: [
                scopedProvider.overrideWithValue(index),
              ],
              child: ListItem(),
            ),
          ),
        ),
      );
}

class ListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Consumer(
        builder: (context, watch, _) {
          final index = watch(scopedProvider);
          return Text(
            '$index',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24),
          );
        },
      );
}
