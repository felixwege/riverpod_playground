import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  // Wrap your app in ProviderScope
  runApp(
    const ProviderScope(child: MyApp()),
  );
}

final futureProvider = FutureProvider<String>((ref) async {
  final future = Future.delayed(
    Duration(seconds: 3),
    () => 'The Future Is Now.',
  );
  return future;
});

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home());
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('FutureProvider Example')),
        body: Center(
          child: Consumer(
            builder: (context, watch, _) {
              final text = watch(futureProvider).data;
              // Handle loading and error
              return text?.when(
                    loading: () => const CircularProgressIndicator(),
                    data: (data) => Text(
                      data,
                      style: TextStyle(fontSize: 24),
                    ),
                    error: (err, stack) => Text('Error $err'),
                  ) ??
                  const CircularProgressIndicator();
            },
          ),
        ),
      );
}
