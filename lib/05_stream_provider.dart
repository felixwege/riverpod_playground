import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(
    const ProviderScope(child: MyApp()),
  );
}

// Use StreamProvider to provide a stream
final randomNumberProvider = StreamProvider.autoDispose<int>((ref) async* {
  final rng = Random();
  while (true) {
    await Future.delayed(Duration(seconds: 1));
    yield rng.nextInt(10) + 1;
  }
});

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home());
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('StreamProvider Example')),
        body: Center(
          child: Consumer(
            builder: (context, watch, _) {
              final randomNumber = watch(randomNumberProvider).data;
              return randomNumber?.when(
                    loading: () => const CircularProgressIndicator(),
                    data: (data) => Text(
                      '$data',
                      style: TextStyle(fontSize: 24),
                    ),
                    error: (err, stack) => Text('Error $err'),
                  ) ??
                  const CircularProgressIndicator();
            },
          ),
        ),
      );
}
